﻿using System;
using TechTalk.SpecFlow;
using CalculatorNamespace;
using FluentAssertions;

namespace MyProject.Specs
{
    [Binding]
    public class CalculatorSteps
    {
        private readonly Calculator _calculator = new Calculator();
        private int _result;
        [Given(@"I have entered (.*) in to the Calculator")]
        public void GivenIHaveEnteredInToTheCalculator(int p0)
        {
            _calculator.FirstNumber = p0;
        }
        
        [Given(@"Als i have entered (.*) into the Calculator")]
        public void GivenAlsIHaveEnteredIntoTheCalculator(int p0)
        {
            _calculator.SecondNumber = p0;
        }
        
        [When(@"I press substract")]
        public void WhenIPressSubstract()
        {
            _result = _calculator.Substract();
        }
        
        [Then(@"the answer should be (.*) on the screen")]
        public void ThenTheAnswerShouldBeOnTheScreen(int p0)
        {
            _result.Should().Be(p0);
        }
    }
}
